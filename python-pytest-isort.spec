%global srcname pytest-isort

Name:          python-%{srcname}
Summary:       py.test plugin to check import ordering using isort
License:       BSD
URL:           https://github.com/moccu/pytest-isort/

Version:       0.3.1
Release:       1%{?dist}
Source0:       %{pypi_source}

BuildArch:     noarch


%global _description %{expand:
py.test plugin to check import ordering using isort.}

%description %_description


%package -n python3-%{srcname}
Summary:       %{summary}

BuildRequires: python3-devel
BuildRequires: %{py3_dist isort} >= 4.0
BuildRequires: %{py3_dist pytest} >= 3.5
BuildRequires: %{py3_dist setuptools}

Requires:      %{py3_dist isort} >= 4.0
Requires:      %{py3_dist pytest} >= 3.5

%{?python_provide:%python_provide python3-%{srcname}}


%description -n python3-%{srcname} %_description


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py3_build


%install
%py3_install


%check
py.test


%files -n python3-%{srcname}
%doc README.rst
%license LICENSE.rst
%{python3_sitelib}/pytest_isort.py
%{python3_sitelib}/pytest_isort-%{version}-py*.egg-info
%{python3_sitelib}/__pycache__/*


%changelog
* Fri Nov 01 2019 Mathieu Bridon <bochecha@daitauha.fr> - 0.3.1-1
- Initial package for Fedora.
